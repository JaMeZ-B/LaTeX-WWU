%!TEX root = ../MC_SS17.tex
\section{Semantisches Model-Checking}
\label{sec:para4}
\nextlecture

Sei $\kts = (S, S_0, R, L)$ eine (totale) Kripke-Struktur.

Idee: Induktiv über den Formelaufbau die \enquote{Semantik} der Forml $[[\phi]]_k = \{s \in S \mid s \models \phi \}$ ausrechnen.

Hauptanwendungsgebiet: Branching-Time Logiken, insbesondere CTL (und modale $\mu$-Kalkül).


\begin{beob}
	Alle CTL-Formeln sind Zustandsformeln (sieht man leicht mit struktureller Induktion). Deshalb kann man die Bedeutung von CTL-Formeln(!) direkt induktiv auf Zuständen definieren. Für Basis-CTL sieht dies wie folgt aus: Sei $s \in S$
	
	\begin{itemize}
		\item $s \models p \text{ gdw. } p \in L(s)$
		\item $s \models \phi_1 \land \phi_2 \text{ gdw. } s \models \phi_1 \text{ und } s \models \phi_2$
		\item $s \models \lnot \phi \text{ gdw. nicht } s \models \phi$
		\item $s \models \E \X \phi \text{gdw. } \exists s' \in S: (s, s') \in R \text{ und } s' \models \phi$
		\item $s \models \E(\phi_1 \U \phi_2) \text{ gdw. } \exists s_0, s_1, s_2, \dots: s_0 = s \land (\forall i: (s_i, s_{i+1}) \in R) \land \exists k: s_k \models \phi_2 \land \forall l: 0 \leq l < k \Rightarrow s_l \models \phi_1$
		\item $s \models \A(\phi_1 \U \phi_2) \text{ gdw. } \forall s_0, s_1, s_2, \dots : (s_0 = s \land (\forall i : (s_i, s_{i+1}) \in R) \Rightarrow \exists k: s_k \models \phi_2 \land \forall l: 0 \leq l < k \Rightarrow s_l \models \phi_1)$
	\end{itemize}
\end{beob}

Wir wollen nun einen Algorithmus zum CTL-Model-Checking betrachten:

\begin{algorithmic}
	\Procedure{CTLModelCheck}{$\kts, \phi$}
		\State Transformiere $\phi$ in äquivalente Basis-CTL-Formel (nach Aufgabe 8.1)
		\State \Call{marking}{$\phi'$}
		\State $\texttt{mc} := \texttt{true}$
		\For{$s \in S_0$}
			\State $\texttt{mc} := \texttt{mc} \land s.\phi'$
		\EndFor
		\State \Return $\texttt{mc}$
	\EndProcedure
\end{algorithmic}

Kern des CTL-Modelcheckers ist die Prozedur $\texttt{marking}$. Sie arbeitet wie folgt:
\begin{itemize}
	\item markiert jeden Zustand $s \in S$ mit genau den Teilformeln $\psi$ von $\phi'$ mit $s \models \psi$.
	\item verwendet dazu Attribute $s.\psi$
	\begin{itemize}
		\item $s.\psi = \texttt{true}, \text{falls } s \models \psi$
		\item $s.\psi = \texttt{false}, \text{falls } s \not{\models} \psi$
	\end{itemize}
\item berechnet diese Attribute induktiv über den Formelaufbau.
\end{itemize}


\begin{algorithmic}
	\Procedure{marking}{$\phi$}
		\If{$\phi = p$ für ein $p \in AP$}
			\For{$s \in S$}
				\If{$p \in L(s)$}
					\State $s.\phi := \texttt{true}$
				\Else
					\State $s.\phi := \texttt{false}$
				\EndIf
			\EndFor
		\ElsIf{$\phi = \phi_1 \land \phi_2$}
			\State \Call{marking}{$\phi_1$}
			\State \Call{marking}{$\phi_2$}
			\For{$s \in S$}
				\State $s.\phi := s.\phi_1 \land s.\phi_2$
			\EndFor
		\ElsIf{$\phi = \lnot \phi_1$}
			\State \Call{marking}{$\phi_1$}
			\For{$s \in S$}
				\State $s.\phi := \text{not }s.\phi_1$
			\EndFor
		\ElsIf{$\phi = \E\X \phi_1$}
			\State \Call{marking}{$\phi_1$}
			\For{$s \in S$}
				\State $s.\phi := \texttt{false}$
				\For{$(s, s') \in R$}
					\If{$s'.\phi_1 = \texttt{true}$}
						\State $s.\phi = \texttt{true}$
					\EndIf
				\EndFor
			\EndFor
		\ElsIf{$\phi = \E(\phi_1 \U \phi_2)$}
			\State \Call{marking}{$\phi_1$}
			\State \Call{marking}{$\phi_2$}
			\For{$s \in S$}
				\State $s.\phi := \texttt{false}$
				\State $s.\textit{seen} := \texttt{false}$
			\EndFor
			\State $L := \emptyset$ \Comment{Workset}
			\For{$s \in S$}
				\If{$s.\phi_2 = \texttt{true}$}
					\State $L := L \cup \{s\}$
					\State $s.\textit{seen} = \texttt{true}$
				\EndIf
			\EndFor
			\While{$L \neq \emptyset$}
				\State Wähle $s \in L$
				\State $s.\phi = \texttt{true}$
				\For{$(s', s) \in R$}
					\If{$s'.\textit{seen} = \texttt{false}$}
						\State $s'.\textit{seen} = \texttt{true}$
						\If{$s'.\phi_1' = \texttt{true}$}
							\State $L := L \cup \{s'\}$
						\EndIf
					\EndIf
				\EndFor
			\EndWhile
		\ElsIf{$\phi = \A(\phi_1 \U \phi_2)$}
			\State \Call{marking}{$\phi_1$}
			\State \Call{marking}{$\phi_2$}
			\State $L := \emptyset$ \Comment{Workset}
			\For{$s \in S$}
				\State $s.nb := \text{Grad}(s)$ \Comment{Anzahl der NF von s, von denen noch nicht bekannt ist, dass sie $\phi$ erfüllen}
				\State $s.\phi := \texttt{false}$
				
				\If{$s.\phi_2 = \texttt{true}$}
					\State $L := L \cup \{s\}$
				\EndIf
			\EndFor
			\While{$L \neq \emptyset$}
				\State Wähle $s \in L$
				\State $s.\phi = \texttt{true}$
				\For{$(s', s) \in R$}
					\State $s'.nb = s'.nb - 1$
					\If{$s'.nb = 0$ und $s'.\phi_1 = \texttt{true}$ und $s'.\phi_2 = \texttt{false}$}
						\State $L := L \cup \{s'\}$
					\EndIf
				\EndFor
			\EndWhile
		\EndIf
	\EndProcedure
\end{algorithmic}

\begin{lemma*}
	Sei $\phi$ eine Basis-CTL-Formel. Dann gilt nach Abarbeitung des Aufrufs $\texttt{marking}(\phi)$ für alle $s \in S: s.\phi = \texttt{true} \Leftrightarrow s \models \phi$.
\end{lemma*}

\begin{beweis}
	Der Beweis funktioniert über struktureller Induktion über $\phi$, Details werden an dieser Stelle ausgelassen.
	
	Zu Fall 6: $\phi = \A(\phi_1 \U \phi_2)$. Nach Induktionsannahme sind die Werte $s.\phi_1$ und $s.\phi_2$ korrekt gesetzt.
	
	\begin{enumerate}[a)]
		\item \enquote{Wird $s.\phi$ auf \texttt{true} gesetzt, so gilt $s \models \phi$}
		\item[] Beweis mit Induktion über die Anzahl der ausgeführten Anweisungen $s.\phi := \texttt{true}$.
		\item \enquote{Wird $s.\phi$ \textbf{nicht} auf \texttt{true} gesetzt, so gilt $s \not{\models} \phi$} - Beachte: $s.\phi_2 = \texttt{false}$, falls $s.\phi = \texttt{false}$.
		\item[] Bleibt $s.\phi = \texttt{false}$, so ist entweder $s.\phi_1 = \texttt{false}$ (und damit $S \not{\models} \A(\phi_1 \U \phi_2)$) oder $s.nb$ ist nie $0$ geworden. Im letzten Fall existiert ein Nachfolger $s'$ von $s$ mit $s'.\phi = \texttt{false}$. Für $s'$ können wir analog argumentieren. Wir konstruieren so induktiv einen Pfad:
		\begin{center}
			\begin{tikzpicture}
				\node[draw, circle, minimum height = 1.5mm](1) at (0,0) {};
				\node[draw, circle, minimum height = 1.5mm](2) at (1,0) {};
				\node[circle, minimum height = 1.5mm](3) at (2,0) {$\dots$};
				\node[draw, circle, minimum height = 1.5mm](4) at (3,0) {};
				
				\node[]() at (0,-0.5) {$\phi_1$};
				\node[]() at (0,-1) {$\lnot \phi_2$};
				\node[]() at (1,-0.5) {$\phi_1$};
				\node[]() at (1,-1) {$\lnot \phi_2$};
				\node[]() at (3,-0.5) {$\lnot \phi_1$};
				\node[]() at (3,-1) {$\lnot \phi_2$};
				
				\path[->]
				(1) edge[] (2)
				(2) edge[] (3)
				(3) edge[] (4);
			\end{tikzpicture}
		\end{center}
		oder
		\begin{center}
			\begin{tikzpicture}
			\node[draw, circle, minimum height = 1.5mm](1) at (0,0) {};
			\node[draw, circle, minimum height = 1.5mm](2) at (1,0) {};
			\node[circle, minimum height = 1.5mm](3) at (2,0) {$\dots$};
			\node[draw, circle, minimum height = 1.5mm](4) at (3,0) {};
			
			\node[]() at (0,-0.5) {$\phi_1$};
			\node[]() at (0,-1) {$\lnot \phi_2$};
			\node[]() at (1,-0.5) {$\phi_1$};
			\node[]() at (1,-1) {$\lnot \phi_2$};
			\node[]() at (3,-0.5) {$\phi_1$};
			\node[]() at (3,-1) {$\lnot \phi_2$};
			
			\path[->]
			(1) edge[] (2)
			(2) edge[] (3)
			(3) edge[] (4);
			\end{tikzpicture}
		\end{center}
		\item[] In beiden Fällen haben wir einen Pfad von $S$ konstruiert, der $\phi_1 \U \phi_2$ nicht erfüllt. Also $S \not{\models} \A(\phi_1 \U \phi_2)$.
	\end{enumerate}
	\qed
\end{beweis}

\begin{satz}
	CTL-Modelchecking kann in Zeit $\mathcal{O}(|\kts| \cdot |\phi|)$ durchgeführt werden. Dabei ist $|\kts| = |S| + |R|$ und $|\phi| = $ Anzahl der Teilformeln von $\phi$.
\end{satz}

\begin{beweis}
	Der Einzelaufwand für jede der $|\phi|$ Teilformeln von $\phi$ ist jeweils von der Komplexität $\mathcal{O}(|\kts|)$. Gesamtaufwand ist somit $\mathcal{O}(|\kts| \cdot |\phi|)$
\end{beweis}

Die beschriebene Methode geht zurück auf Clark, Emerson und Sistla aus dem Jahre 1986. Einen ähnlichen Ansatz haben Queille und Sifakis 1982 beschrieben. Wegen ihrer guten Komplexität weckte die Methode das Interesse an Model Checking. Clark, Emerson und Sifakis haben im Jahr 2007 den Turing-Preis erhalten.
\cleardoubleoddemptypage