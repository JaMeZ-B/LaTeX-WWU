%!TEX root = ../ZZT_SS17.tex
\section{Zahlen}
\label{sec:5}

Wir betrachten zum Abschluss den Aufbau des Zahlsystems:
\[
	\NN \subseteq \ZZ \subseteq \QQ \subseteq \RR \subseteq \CC
\]
Wir nehmen dazu an, wir \enquote{kennen} $\NN = \{0,1,2,\dots\}$, das heißt wir vermeiden die Frage, was eine natürliche Zahl ist.

\begin{kons}[Ganze Zahlen]
	\label{kon:5.1}
	Motivation: Wir wollen Gleichungen der Form $x + b = a$ lösen können, und zwar für alle $a,b \in \NN$, also auch $x + 7 = 2$. \marginnote{[1]} 
	Setze $\mathcal{Z} = \{(a,b) : a,b \in \NN\}$ und definiere \index{ganze Zahl}
	\[
		(a,b) \sim (a',b') \quad :\Leftrightarrow \quad a+b' = a'+b.
	\]
	Das ist eine Äquivalenzrelation: Reflexivität und Symmetrie sind klar.
	Ist $(a,b) \sim (a',b')$ und $(a',b') = (a'',b'')$, so gilt $a+b' = a'+b$ und $a'+b'' = a''+b'$.
	Addition beider Seiten, Ausnutzen der Kommutativität von \enquote{$+$} und Kürzen liefert dann $a + b'' = a'' + b$, also $(a,b) \sim (a'',b'')$.
	
	Wir schreiben $a-b$ für die Äquivalenzklasse von $(a,b)$ und setzen $a \in \NN$ mit $a-0$ gleich.
	Definiere die Verknüpfung \enquote{$+$} auf solchen Klassen:
	\[
		(a_1-b_1) + (a_2-b_2) := (a_1+a_2)-(b_1+b_2)
	\]
	Wegen
	\[
		a_1 + b_1' = a_1' + b_2 \quad \wedge \quad a_2 + b_2' = a_2' + b_2 \qquad \Rightarrow \qquad (a_1+a_2,b_1 + b_2) \sim (a_1' + a_2', b_1' + b_2')
	\]
	ist diese wohldefiniert.
	Weiter rechnet man nach, dass sie assoziativ und kommutativ ist, wobei $0-0$ das neutrale Element ist.
	
	Definiere weiter die Verknüpfung \enquote{$\cdot$} durch
	\[
		(a_1-b_1) \cdot (a_2-b_2) := (a_1a_2 + b_1b_2) - (a_1b_2 + a_2b_1)
	\]
	und setze $\ZZ := \{a-b : a,b \in \NN\}$.
	Mit vielen elementaren Rechnungen zeigt man, dass $(\ZZ,+,\cdot)$ ein kommutativer Ring ist.	
\end{kons}

\begin{kons}[Rationale Zahlen]
	\label{kons:5.2}
	Setze $\mathcal{Q} := \{(a,b) : a,b \in \ZZ, b \neq 0\}$ und definiere die Relation \marginnote{[2]}
	\[
		(a,b) \approx (a',b') \quad :\Leftrightarrow \quad ab' = a'b.
	\]
	Zeige, dass $\approx$ eine Äquivalenzrelation auf $\mathcal{Q}$ ist und schreibe $\frac{a}{b}$ für die Äquivalenzklasse von $(a,b)$ bezüglich $\approx$.
	Rechne dann nach, dass \index{rationale Zahl}
	\[
		\frac{a_1}{b_1} + \frac{a_2}{b_2} := \frac{a_1b_2 + a_2b_1}{b_1b_2} \qquad \qquad \qquad \frac{a_1}{b_1} \cdot \frac{a_2}{b_2} := \frac{a_1a_2}{b_1b_2}
	\]
	wohldefinierte Verknüpfungen sind, und dass die Menge
	\[
		\QQ := \penb{\frac{a}{b} : a,b \in \ZZ, b \neq 0}
	\]
	mit den Verknüpfungen $+$,$\cdot$ ein Körper ist.
\end{kons}

\begin{kons}[Reelle Zahlen]
	\label{kons:5.3}
	Wir betrachten rationale Folgen $(q_n)_{n \geq 0}, q_n \in \QQ$. \marginnote{[3]}
	Eine rationale Folge $(q_n)_{n \geq 0}$ heißt \index{reelle Zahl}
	\begin{enumerate}[(a)]
		\item \textbf{beschränkt}, falls es ein $K \in \QQ$ gibt, sodass $\abs{q_n} \leq K$ für alle $n \geq 0$ gilt. \index{Folge!beschränkt}
		\item \textbf{Cauchyfolge}, falls es zu jedem $\varepsilon \in \QQ, \varepsilon > 0$ ein $N = N(\varepsilon) \in \NN$ gibt, sodass für alle $i,j \geq N(\varepsilon)$ gilt: $\abs{q_j - q_i} \leq \varepsilon$. \index{Folge!Cauchyfolge}
		\item \textbf{Nullfolge}, falls es zu jedem $\varepsilon \in \QQ, \varepsilon > 0$ ein $M(\varepsilon) \in \NN$ gibt, sodass für alle $i \geq M(\varepsilon)$ gilt: $\abs{q_i} \leq \varepsilon$. \index{Folge!Nullfolge}
	\end{enumerate}
	Für rationale Folgen $(q_n)_n$, $(r_n)_n$ definiere
	\[
		(q_n)_n + (r_n)_n := (q_n+r_n)_n \qquad \qquad \qquad (q_n)_n \cdot (r_n)_n := (q_n r_n)_n
	\]
	Man rechnet leicht nach:
	Mit diesen Verknüpfungen wird die Menge aller rationalen Folgen ein kommutativer Ring $F$ mit Nullelement $(0,0,0,\dots)$ und Einselement $(1,1,1,\dots)$.
	Wir können $\QQ$ als Teilring auffassen mit
	\[
		q \longmapsto (q,q,q,\dots).
	\]
\end{kons}

\begin{lemma}
	\label{lemma:5.4}
	\begin{enumerate}[(i)]
		\item Jede Cauchyfolge ist beschränkt. \marginnote{[4] \\ 6.7.}
		\item Jede Nullfolge ist Cauchyfolge.
		\item Die beschränkten Folge bilden einen Teilring $B \subseteq F$ und die Cauchyfolgen bilden ein Teilring $C \subseteq B \subseteq F$.
	\end{enumerate}
\end{lemma}

\begin{beweis}
	\begin{enumerate}[(i)]
		\item Sei $(q_n)_n$ eine Cauchyfolge, sei $N = N(1)$ und sei $K_1 = \max\{ \abs{q_j} : 0 \leq j \leq N(1)\}$, $K_2 = \abs{q_n}+1$ und $K = \max\{K_1,K_2\}$.
		Für alle $i \in \NN$ gilt $\abs{q_i} \leq K$.
		\item Setze $N(\varepsilon) = M\enb{\frac{\varepsilon}{2}}$.
		ISt $i,j \geq M\enb{\frac{\varepsilon}{2}}$, so folgt $\abs{q_i}, \abs{q_j} \leq \frac{\varepsilon}{2} = \abs{q_i - q_j} \leq \varepsilon$.
		\item Ist $(q_n)_{n}$ durch $K$ beschränkt und $(r_n)_n$ durch $L$ beschränkt, so ist $(q_n)_n + (r_n)_n$ durch $K+L$ beschränkt und $(q_n)_n \cdot (r_n)_n$ durch $KL$ beschränkt.
		Das Einselement $(1,1,1,\dots)$ ist offensichtlich beschränkt und mit $(q_n)_n$ ist auch $(-q_n)_n$ beschränkt.
		Folglich ist $B$ ein Teilring von $F$.
		
		Zeige nun, dass $C \subseteq B$ ein Teilring ist.
		Sei $\varepsilon > 0$ und $(q_n)_n$ sowie $(r_n)_n$ rationale Cauchyfolgen.
		\begin{itemize}
			\item Wähle $N \geq 0$ so, dass $\abs{q_i-q_j}, \abs{r_i-r_j} \leq \frac{\varepsilon}{2}$ für alle $i,j \geq N$.
			Es folgt $\abs{(q_i + r_i) - (q_j + r_j)} \leq \varepsilon$ für alle $i,j \geq N$, also $(q_n)_n + (r_n)_n \in C$.
			\item Weiter ist $(-q_n)_n, (1,1,1,\dots)$ und $(0,0,0,\dots) \in C$.
			\item Wähle $K \geq 1$ so, dass $\abs{q_i}, \abs{r_i} \leq K$ für alle $i \geq 0$ sowie $N \geq 0$ so, dass $\abs{q_i-q_j}, \abs{r_i-r_j} \leq \frac{\varepsilon}{2K}$ für alle $i,j \geq N$.
			Es folgt
			\[
				\abs{q_ir_i - q_jr_j} = \abs{q_ir_i - q_ir_j + q_ir_j - q_jr_j} \leq \abs{q_i} \cdot \abs{r_i-r_j} + \abs{r_j} \cdot \abs{q_i-q_j} \leq K \cdot \frac{\varepsilon}{2K} + K \cdot \frac{\varepsilon}{2K} = \varepsilon,
			\]
			also $(q_n)_n \cdot (r_n)_n \in C$.
		\end{itemize}
	\end{enumerate}
\end{beweis}

\begin{satz}
	\label{satz:5.5}
	$N \subseteq C$ ist ein Ideal und $C \diagup N$ ist ein Körper. \marginnote{[5]}
\end{satz}

\begin{beweis}
	Es gilt $(0,0,0,\dots) \in N$.
	Summen und Differenz von Nullfolgen sind wieder Nullfolgen, also ist $N \subseteq C$ Untergruppe.
	Ist $(q_n)_n \in C \subseteq B$ und $(r_n)_n \in N$, so ist $(q_n)_n \cdot (r_n)_n$ wieder eine Nullfolge, da Cauchyfolgen beschränkt sind.
	Also gilt $N \NT G$ (sogar $N \NT B$).
	
	Sei nun $(q_n)_n \in C$ keine Nullfolge.
	Dann gibt es $\varepsilon > 0$ und $N \geq 0$ so, dass $\abs{q_i} \geq 2\varepsilon$ für alle $j \geq N$.
	Setze
	\[
		r_n = \begin{cases}
			1, & \text{falls } n > N \\
			\frac{1}{q_n}, & \text{falls } n \geq N.
		\end{cases}
	\]
	Dann ist $(r_n)_n$ keine Nullfolge, aber eine Cauchyfolge, und es gilt
	\[
		(r_n)_n \cdot (q_n)_n = (q_0,\dots,q_{N-1},1,1,1,\dots) = (1,1,1,\dots) + \underbrace{(q_0-1, q_1-1, \dots, q_{N-1}-1,0,0,0,\dots)}_{\in N},
	\]
	folglich $\enb{(r_n)_n + N} \cdot \enb{(q_n)_n + N} = (1,1,1,\dots) + N$.
	Wir setzen $\RR:= C \diagup N$.
	
	Jetzt kann man nachrechnen: $(\RR,+,\cdot)$ ist Körper mit Anordnung
	\[
		x \geq 0 \quad :\Leftrightarrow \quad \text{es gibt } y \in \RR \text{ mit } x = y^2
	\]
	und der \Index{Supremumseigenschaft}:
	Zu jeder nicht leeren, nach oben beschränkten Teilmenge $X \subseteq \RR$ existiert eine kleinste obere Schranke $\xi = \sup(X) \in \RR$.
	Mehr Details: \cite[§1.5]{HewittStromberg}.
\end{beweis}

\begin{kons}[Komplexe Zahlen]
	\label{kons:5.6}
	Wir definieren $\CC = \RR^2$ ($x$-$y$-Ebene) mit Basis $1 = (1,0), i = (0,1)$. \marginnote{[6]}
	Jede komplexe Zahl ist also von der Form $z = x \cdot 1 + y \cdot i$ mit $x,y \in \RR$.
	Man nennt $x = \Re(z)$ den \Index{Realteil} und $y = \Im(z)$ den \Index{Imaginärteil} von $z$.
	Die Multiplikation wird erklärt durch \index{komplexe Zahl}
	\[
		(a \cdot 1 + b \cdot i) \cdot (c \cdot 1 + d \cdot i) = (ac-bd) \cdot 1 + (bc+ad) \cdot i.
	\]
	Der Absolutbetrag von $z$ ist $\abs{z} = \sqrt{x^2+y^2}$.
	Man rechnet nach: $(\CC,+,\cdot)$ ist ein (kommutativer) Körper mit Einselement $1 = 1 \cdot 1 + 0 \cdot i$ und Nullelement $0 = 0 \cdot 1 + 0 \cdot i$.
	Es gilt $i \cdot i = -1$ sowie $\abs{z \cdot w} = \abs{z} \cdot \abs{w}$.
	Weiter ist $\CC$ algebraisch abgeschlossen, das heißt zu jedem Polynom $f \in \CC[T]$ mit $\deg(f) \geq 1$ gibt es ein $z \in \CC$ mit $f(z) = 0$, also $f = (T-z) \cdot h$ nach Satz~\ref{satz:4.8}.
\end{kons}

\begin{center}
\begin{tikzpicture}[scale=1.5]
	\draw[thick,->] (-1.1,0) -- (1.5,0);
	\draw[thick,->] (0,-1.1) -- (0,1.5);
	
	\draw[ultra thick,fbblau,->] (0,0) -- (1,0) node[anchor=south east]{$1$};
	\draw[ultra thick,fbblau,->] (0,0) -- (0,1) node[anchor=north west]{$i$};
	\draw (0,0) node[anchor=north east]{$0$};
	\draw (-1,0) node[below]{$-1$};
	\draw (1,0) node[below]{$1$};
	\draw (0,-1) node[left]{$-1$};
	\draw (0,1) node[left]{$1$};
	\draw (-1,-.05) -- (-1,.05);
	\draw (1,-.05) -- (1,.05);
	\draw (-.05,1) -- (.05,1);
	\draw (-.05,-1) -- (.05,-1);
\end{tikzpicture}
\end{center}

Die Gemeinsamkeit der Konstruktion von $\ZZ, \QQ, \RR$ ist: \marginnote{10.7.}
Wir definieren die \enquote{fehlenden} Elemente als Äquivalenzklassen derjenigen Gleichungen, die sie lösen sollen:
negative Zahlen als Äquivalenzklassen von $a = x+b$, rationale Zahlen als Äquivalenzklassen von $a = bx$, relle Zahlen als Äquivalenzklassen von Cauchyfolgen.
Bleibt die Frage:
Was sind natürliche Zahlen?
Antwort der Mengenlehre:

Ist $a$ eine Menge, so sei $a^+ := a \cup \{a\}$.
Die Konstruktion von $\NN$ erfolgt rekursiv:
\begin{align*}
	0 &:= \emptyset \\
	1 &:= 0^+ = \emptyset \cup \{ \emptyset\} \\
	2 &:= 1^+ = \{ \emptyset \} \cup \{ \{\emptyset\} \} \\
	3 &:= 2^+ = \{\emptyset, \{ \emptyset\} \} \cup \{ \{ \emptyset, \{ \emptyset \} \} \} = \{ \emptyset, \{ \emptyset \}, \{ \emptyset, \{ \emptyset\}\}\} \\
	&\vdots \\
	n+1 &:= n^+
\end{align*}
Es ist mühsam, dann nachzurechnen, dass man mit $n+k := ((((n+\underbrace{1)+1)+1)+ \dots) + 1}_{k \text{ Mal}}$ eine assoziative Verknüpfung erhält.
\cleardoubleoddemptypage