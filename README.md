[![build status](https://gitlab.com/JaMeZ-B/LaTeX-WWU/badges/master/build.svg)](https://gitlab.com/JaMeZ-B/LaTeX-WWU/commits/master)
# LaTeX-WWU
lecture notes written at WWU Münster, FB10. This should be migrated into a more official collection of lecture notes at some point.

---
Einen Ordner mit allen Skripten findet man auf 
**[SCIEBO](https://uni-muenster.sciebo.de/public.php?service=files&t=965ae79080a473eb5b6d927d7d8b0462)**.

Die folgenden Skripte sind verfügbar (die Links führen direkt zu den einzelnen PDF-Dateien)

* SoSe 2020
	* [Topologische Gruppen](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/TG_SS20/TG_SS20.pdf?job=build_deploy_all "Topologische Gruppen") (Leon)
* WiSe 2018
	* [Logik 3](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Logik3_WS18/logik3.pdf?job=build_deploy_all "Logik 3") (Leon)
* SoSe 2018
	* [Komplexitätstheorie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/KoT_SS18/komplexitaetstheorie.pdf?job=build_deploy_all "Komplexitätstheorie") (Stefanie)
* WiSe 2017
	* [Coarse Index Theory](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/CoarseIndex_WS17/coarse_index_theory.pdf?job=build_deploy_all "Coarse Index Theory") (Jannes)
* SoSe 2017
	* [Model-Checking](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/MC_SS17/MC_SS17.pdf?job=build_deploy_all "Model-Checking") (Florian)
	* [Zahlen und elementare Zahlentheorie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/ZZT_SS17/ZZT_SS17.pdf?job=build_deploy_all "Zahlen und elementare Zahlentheorie") (Phil)
	* [Topologie III.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Topo3_SS17/topologie_3.pdf?job=build_deploy_all "Topologie III.") (Jannes)
	* [Differentialtopologie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/DiffTopo_SS17/differentialtopologie.pdf?job=build_deploy_all "Differentialtopologie") (Jannes)
	* [Grundlagen der Analysis, Topologie, Geometrie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/AnaTopGeo_SS17/ana_top_geo.pdf?job=build_deploy_all "Grundlagen der Analysis, Topologie und Geometrie") (Jannes/Arthur Bartels)
* WiSe 2016
	* [Seminar: Madsen-Weiss](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/MadsenW_WS16/madsen_weiss.pdf?job=build_deploy_all "Seminar: Madsen-Weiss") (Jannes; unvollständig)
* SoSe 2016
	* [Operatoralgebren II.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/OpAlg2_SS16/operatoralgebren2.pdf?job=build_deploy_all "Operatoralgebren II.") (Jannes)
	* [Lineare Algebra II.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/LA2_SS16/LA2_SS16.pdf?job=build_deploy_all "Lineare Algebra II.") (Phil)
	* [Liegruppen](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/LieGrp_SS16/liegruppen.pdf?job=build_deploy_all "Liegruppen") (Jannes)
	* [Algebraische K-Theorie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/AlgKT_SS16/algebraische_KTheorie.pdf?job=build_deploy_all "Algebraische K-Theorie") (Jannes)
	* [PDGL I.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/PDGL1_SS16/pdeskript.pdf?job=build_deploy_all "PDGL I.") (Tim)
	* [Wahrscheinlichkeitstheorie auf Graphen](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/WTGraph_SS16/WTG.pdf?job=build_deploy_all "Wahrscheinlichkeitstheorie auf Graphen") (Lukas)
* WiSe 2015
	* [CAT(0) kubische Komplexe](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/CAT0_WS15/CAT0_WS1516.pdf?job=build_deploy_all "CAT(0) kubische Komplexe") (Phil)
	* [Topologie II.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Topo2_WS15/topologie_2.pdf?job=build_deploy_all "Topologie II.") (Jannes)
	* [Operatoralgebren](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/OpAlg_WS15/operatoralgebren.pdf?job=build_deploy_all "Operatoralgebren") (Jannes)
	* [Modellreduktion](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/ModRed_WS15/Modellreduktion.pdf?job=build_deploy_all "Modellreduktion") (Tobias)
* SoSe 2015
	* [Elliptische Kurven und Kryptographie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/EKK_SS15/EKK_SS15.pdf?job=build_deploy_all "Elliptische Kurven und Kryptographie") (Phil)
	* [K-Theorie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/KTheorie_SS15/K-Theorie.pdf?job=build_deploy_all "K-Theorie und die Hopf-Invariante") (Jannes; momentan noch etwas unvollständig…)
	* [Analysis II.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Ana2_SS15/Ana2.pdf?job=build_deploy_all "Analysis II.") (Tim)
* WiSe 2014
	* [Topologie I.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Topo1_WS14/topologie_1.pdf?job=build_deploy_all "Topologie I.") (Jannes)
	* [Funktionalanalysis](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/FunkAna_WS14/funktionalanalysis.pdf?job=build_deploy_all "Funktionalanalysis") (Jannes)
	* [Elementare Zahlentheorie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/EZT_WS14/EZT_WS1415.pdf?job=build_deploy_all "Elementare Zahlentheorie") (Phil)
	* [Differentialgeometrie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/DiffGeo_WS14/diff_geo.pdf?job=build_deploy_all "Differentialgeometrie") (Arne)
	* [Einführung in die Algebra](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/EinfAlg_WS14/Einf_Algebra.pdf?job=build_deploy_all "Einführung in die Algebra") (Tobias)
	* [Finanzmathematik](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Fima_WS14/Fima_WS14.pdf?job=build_deploy_all "Finanzmathematik") (Tobias)
* SoSe 2014
	* [Höhere Algebra I.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/HoehAlg1_SS14/hoehere_algebra.pdf?job=build_deploy_all "Höhere Algebra I.") (Jannes)
	* [Differentialformen und Mannigfaltigkeiten](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/DiffMa_SS14/diff_ma.pdf?job=build_deploy_all "Differentialformen und Mannigfaltigkeiten") (Jannes)
	* [Grundlagen der Analysis, Topologie, Geometrie](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/AnaTopGeo_SS14/ana_top_geo.pdf?job=build_deploy_all "Grundlagen der Analysis, Topologie und Geometrie") (Jannes)
	* [Partielle Differentialgleichungen I.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/PDGL1_SS14/PDGL1.pdf?job=build_deploy_all "Partielle Differentialgleichungen I.") (Phil)
* WiSe 2013
	* [Analysis III.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Ana3_WS13/analysis3.pdf?job=build_deploy_all "Analysis III.") (Jannes)
	* [Einführung in die Algebra](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/EinfAlg_WS13/algebra.pdf?job=build_deploy_all "Einführung in die Algebra") (Jannes)
* SoSe 2013
	* [Analysis II.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Ana2_SS13/analysis2.pdf?job=build_deploy_all "Analysis II.") (Jannes)
	* [Lineare Algebra II.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/LA2_SS13/lineare_algebra2.pdf?job=build_deploy_all "Lineare Algebra II.") (Jannes)
* WiSe 2012,  
_Nur der Vollständigkeit wegen hinzugefügt, Qualität bleibt hinter anderen Skripten zurück_
	* [Analysis I.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/Ana1_WS12/analysis1.pdf?job=build_deploy_all "Analysis I.") (Jannes)
	* [Lineare Algebra I.](https://gitlab.com/JaMeZ-B/LaTeX-WWU/-/jobs/artifacts/master/raw/LA1_WS12/lineare_algebra1.pdf?job=build_deploy_all "Lineare Algebra I.") (Jannes)
	
(hauptsächlicher Autor in Klammern)

### Mitarbeit
Ich lade jeden herzlich ein, sich mittels Forks und Pull-Requests an der Entwicklung der Skripte zu beteiligen. Es gibt sicherlich noch diverse Fehler und auch bei den
Zeichnungen fehlt noch eine ganze Menge.
Schon einmal vielen Dank an

* Phil (phist91) _PDGL I., Elementare Zahlentheorie, Elliptische Kurven und Kryptographie, CAT(0) kubische Komplexe, Lineare Algebra II., Zahlen und elementare Zahlentheorie_
* Arne (moregrey) _Differentialformen und Mannigfaltigkeiten, Differentialgeometrie_
* Tobias (Tazdr) _Einführung in die Algebra, Finanzmathematik, Modellreduktion_
* Tim (Tii1) _Analysis II._
* Lukas (B0Bftl) _Wahrscheinlichkeitstheorie auf Graphen_
* Gautam (gdbgauda) _Höhere Algebra I._
* Florian (flokuep) _Model-Checking_

für die Beteiligung an diesem Projekt!


#### Hinweise (technische und rechtliche)
Die TeX-Dateien sind für den Einsatz mit `XeLaTeX` optimiert! Die ältere Variante `mitschrift.tex` ist aber so aufgebaut, dass sie auch mit `pdfLaTeX` kompiliert werden kann. `mitschrift2.tex` und `mitschrift3.tex` nutzt ein spezielles Kommando, dass die meisten Editoren dazu bewegt, automatisch `XeLaTeX` für diese Datei zu benutzen. Alle benutzten Schriftarten sind Bestandteil einer üblichen LaTeX-Installation. Sollten die Dateien Fehler produzieren, ist in 98% der Fälle die LaTeX-Installation nicht auf dem aktuellsten Stand oder in irgend einer Form zerschossen! Dies gilt insbesondere, wenn man LaTeX über die Paketverwaltung von Linux installiert, denn dort stehen meist nur völlig veraltete Versionen bereit.


Die Präambel darf natürlich gerne weiterverwendet werden (GPLv3-Lizenz, siehe `LICENSE`), eine Namensnennung ist nicht nötig … aber ich freue mich natürlich drüber :)
